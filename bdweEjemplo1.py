# Databricks notebook source
# MAGIC %md
# MAGIC 
# MAGIC ## Overview
# MAGIC 
# MAGIC This notebook will show you how to create and query a table or DataFrame that you uploaded to DBFS. [DBFS](https://docs.databricks.com/user-guide/dbfs-databricks-file-system.html) is a Databricks File System that allows you to store data for querying inside of Databricks. This notebook assumes that you have a file already inside of DBFS that you would like to read from.
# MAGIC 
# MAGIC This notebook is written in **Python** so the default cell type is Python. However, you can use different languages by using the `%LANGUAGE` syntax. Python, Scala, SQL, and R are all supported.

# COMMAND ----------

# File location and type
file_location = "/FileStore/tables/googleplaystore.csv"
file_type = "csv"

# CSV options
infer_schema = "false"
first_row_is_header = "true"
delimiter = ","

# The applied options are for CSV files. For other file types, these will be ignored.
df = spark.read.format(file_type) \
  .option("inferSchema", infer_schema) \
  .option("header", first_row_is_header) \
  .option("sep", delimiter) \
  .load(file_location)

display(df)

# COMMAND ----------

# Create a view or table

temp_table_name = "googleplaystore_csv"

df.createOrReplaceTempView(temp_table_name)

# COMMAND ----------

# MAGIC %sql
# MAGIC 
# MAGIC /* Query the created temp table in a SQL cell */
# MAGIC 
# MAGIC select * from `googleplaystore_csv`

# COMMAND ----------

# With this registered as a temp view, it will only be available to this particular notebook. If you'd like other users to be able to query this table, you can also create a table from the DataFrame.
# Once saved, this table will persist across cluster restarts as well as allow various users across different notebooks to query this data.
# To do so, choose your table name and uncomment the bottom line.

permanent_table_name = "googleplaystore_csv"

# df.write.format("parquet").saveAsTable(permanent_table_name)

# COMMAND ----------

from pyspark.sql.functions import *
import re
def convert(name):
    name = name.replace(' ', '')
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


# COMMAND ----------

df = df.select([col(column).alias(convert(column)) for column in df.columns])

# COMMAND ----------

df.write.format("parquet").saveAsTable("googleplaystore_pq")

# COMMAND ----------

df = spark.read.table("googleplaystore_pq")

# COMMAND ----------

from pyspark.sql.functions import avg, col, udf


@udf('string')
def rename_category(category):
    return category.replace('_', ' ')


display(
    df \
  .select(rename_category('category').alias('category'), 'rating') \
  .where(df.rating != 'NaN') \
  .groupBy('category') \
  .agg(avg('rating').alias('avg_rating')) \
  .where(col('avg_rating').isNotNull() & (col('avg_rating') <= 5.0)) \
  .sort('avg_rating')
)
